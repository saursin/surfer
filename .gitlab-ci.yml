image: rust:latest

variables:
    CARGO_HOME: $CI_PROJECT_DIR/cargo
    FF_ENABLE_BASH_EXIT_CODE_CHECK: "true"
    GIT_SUBMODULE_STRATEGY: normal
    EPHEMERAL_BRANCHES_PATH: preview # subpath where previews of pages will be built
    PACKAGE_REGISTRY_URL: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/surfer/${CI_COMMIT_TAG}"

stages:
    - test
    - deploy
    - upload
    - release

clippy:
    stage: test
    before_script:
        - rustup component add clippy
        - cargo install gitlab_clippy
    script:
        - cargo -V
        - cargo clippy --color=always
    after_script:
        - cargo clippy --message-format=json | $CARGO_HOME/bin/gitlab-clippy > gl-code-quality-report.json
    artifacts:
        reports:
            codequality: gl-code-quality-report.json
        expire_in: 1 week
    cache:
        key: test
        paths:
            - target
            - $CARGO_HOME
        policy: pull
    rules:
        - when: always

test:
    stage: test
    script:
        - rustc -V
        - cargo -V
        - cargo test --color=always --locked
    cache:
        key: test
        paths:
            - target
            - $CARGO_HOME
    artifacts:
        paths:
            - snapshots
        expire_in: 1h
        when: on_failure
    rules:
        - when: always

test-no-default-features:
    stage: test
    script:
        - rustc -V
        - cargo -V
        - cargo test --color=always --locked --no-default-features
    cache:
        key: test-no-default-features
        paths:
            - target
            - $CARGO_HOME
    artifacts:
        paths:
            - snapshots
        expire_in: 1h
        when: on_failure
    rules:
        - when: always

test-oldest-supported:
    # This version should match rust-version in Cargo.toml
    image: rust:1.75
    stage: test
    script:
        - rustc -V
        - cargo -V
        - cargo test --color=always --locked
    cache:
        key: test-oldest-supported
        paths:
            - target
            - $CARGO_HOME
    artifacts:
        paths:
            - snapshots
        expire_in: 1h
        when: on_failure
    rules:
        - when: always

test-beta:
    image: "instrumentisto/rust:beta"
    stage: test
    script:
        - rustc -V
        - cargo -V
        - cargo test --color=always --locked
    cache:
        key: test-beta
        paths:
            - target
            - $CARGO_HOME
    artifacts:
        paths:
            - snapshots
        expire_in: 1h
        when: on_failure
    rules:
        - when: always
    allow_failure: true

test-nightly:
    image: "rustlang/rust:nightly"
    stage: test
    script:
        - rustc -V
        - cargo -V
        - cargo test --color=always --locked
    cache:
        key: test-nightly
        paths:
            - target
            - $CARGO_HOME
    artifacts:
        paths:
            - snapshots
        expire_in: 1h
        when: on_failure
    rules:
        - when: always
    allow_failure: true

test-macos-aarch64:
    stage: test
    variables:
        HOMEBREW_NO_AUTO_UPDATE: 1
        HOMEBREW_NO_ENV_HINTS: 1
        HOMEBREW_NO_INSTALL_CLEANUP: 1
    tags:
        - saas-macos-medium-m1
    image: macos-14-xcode-15
    before_script:
        - brew install rustup-init
        - ulimit -n 1000
        - rustup install 1.77
        - rustup override set 1.77
        - cargo -V
        - rustc -V
    script:
        - cargo test
    cache:
        key: test-macos-aarch64
        paths:
            - target
            - $CARGO_HOME
    rules:
        - if: '$CI_PROJECT_NAMESPACE == "surfer-project"'
    allow_failure: true

coverage:
    image: xd009642/tarpaulin
    stage: test
    interruptible: true
    cache:
        key: coverage
        paths:
            - target
            - $CARGO_HOME
    script:
        - rustc -V
        - cargo -V
        - shopt -s globstar
        - cargo tarpaulin --out Xml --root . --manifest-path Cargo.toml --exclude-files spade/**/* fzcmd/**/* f128/**/* --skip-clean --color Always
    coverage: '/^\d+.\d+% coverage/'
    artifacts:
        reports:
            coverage_report:
                coverage_format: cobertura
                path: cobertura.xml
        expire_in: 1 week
    rules:
        - when: always
        - allow_failure: true # Often runs out of disk space


cargo-fmt:
    stage: test
    before_script:
        - rustup component add rustfmt
    script:
        # pass --check to rustfmt to error if un-formatted
        - cargo fmt -- --check
    rules:
        - when: always

typos-and-todos:
    stage: test
    cache: [] # Disable cache since the spell checker finds typos in other peoples source code
    before_script:
        - apt update
        - apt install -y wget
        - wget https://github.com/crate-ci/typos/releases/download/v1.16.25/typos-v1.16.25-x86_64-unknown-linux-musl.tar.gz -O typos.tar.gz
        # Extract only the typos executable to not get a docs folder which the typo checker
        # will find typos in
        - tar xzf typos.tar.gz ./typos
    script:
        - ./typos src
          # invert exit code of grep while still printing all matches
        - set -e; find src -name "*.rs" | xargs grep -Ei "// *TODO" || exit 0 && exit 1
    after_script:
        - >
            if [ $CI_JOB_STATUS == 'success' ]; then
              echo 'SUCCESS'
            else
              echo 'Running again to give you all output since the test failed'
              ./typos src || echo ""
              set -e; find src -name "*.rs" | xargs grep -Ei "// *TODO" || echo ""
            fi
    rules:
        - when: always

linux_build:
    stage: deploy
    script:
        - apt-get update -y
        - apt-get install -y openssl libssl-dev zip
        - git fetch --tags
        - rustc -V
        - cargo -V
        - cargo build --release --locked
        - cp target/release/surfer surfer
        - zip surfer_linux.zip surfer
    artifacts:
        paths:
            - surfer_linux.zip
        expire_in: 1 week
    cache:
        key: linux_build
        paths:
            - target
            - $CARGO_HOME
    rules:
        - if: $CI_COMMIT_BRANCH == "main"
          when: always
        - if: $CI_COMMIT_TAG
          when: always
        - when: never

windows_build:
    stage: deploy
    script:
        - apt-get update -y
        - apt-get install -y mingw-w64 zip
        - git fetch --tags
        - rustc -V
        - cargo -V
        - rustup target add x86_64-pc-windows-gnu
        - cargo build --target x86_64-pc-windows-gnu --release --locked
        - cp target/x86_64-pc-windows-gnu/release/surfer.exe surfer.exe
        - zip surfer_win.zip surfer.exe
    artifacts:
        paths:
            - surfer_win.zip
        expire_in: 1 week
    cache:
        key: windows_build
        paths:
            - target
            - $CARGO_HOME
    rules:
        - if: $CI_COMMIT_BRANCH == "main"
          when: always
        - if: $CI_COMMIT_TAG
          when: always
        - when: never

macos-aarch64_build:
    stage: deploy
    variables:
        HOMEBREW_NO_AUTO_UPDATE: 1
        HOMEBREW_NO_ENV_HINTS: 1
        HOMEBREW_NO_INSTALL_CLEANUP: 1
    tags:
        - saas-macos-medium-m1
    image: macos-14-xcode-15
    before_script:
        - brew install rustup-init
        - rustup install 1.77
        - rustup override set 1.77
        - cargo -V
        - rustc -V
        - git fetch --tags
    script:
        - cargo build --release --locked
        - ls -alrt target/release/surfer
        - cp target/release/surfer surfer
        - zip surfer_macos-aarch64.zip surfer
    artifacts:
        paths:
            - surfer_macos-aarch64.zip
        expire_in: 1 week
    cache:
        key: macos-aarch64_build
        paths:
            - target
            - $CARGO_HOME
    rules:
        - if: $CI_COMMIT_BRANCH == "main"
          when: always
        - if: $CI_COMMIT_TAG
          when: always
        - when: never

pages_build:
    stage: test
    cache:
        key: pages_build
        paths:
            - target
            - $CARGO_HOME
    before_script:
    script:
        - git fetch --tags
        - rustup target add wasm32-unknown-unknown
        - rustc -V
        - cargo -V
        - wget -qO- https://github.com/trunk-rs/trunk/releases/download/v0.19.2/trunk-x86_64-unknown-linux-gnu.tar.gz | tar -xzf-
        - ls ./trunk
        - chmod +x trunk
          # https://github.com/thedodd/trunk/pull/361#issuecomment-1308487648 trunk can not
          # generate relative urls which we want for external pipelines to be able
          # to download our artefacts and put them wherever on their webserver. To fix this,
          # we'll build with a dummy public URL, then replace it with ./
        - RUSTFLAGS="--cfg=web_sys_unstable_apis" ./trunk build --release --public-url /dist
        - cp -r dist pages_build
        - cp examples/*.vcd pages_build/
          # We have to do this from the source file instead of public/ because sed
          # does not replace in place
        - sed -e "s|/dist/|./|g" dist/index.html > pages_build/index.html
    rules:
        - when: always
    artifacts:
        paths:
            - pages_build
        expire_in: 1h

# https://k33g.gitlab.io/articles/2020-07-23-GITLAB-PAGES-EN.html
# Deploy gitlab pages
# The name of this job *must* be pages:, otherwise no deploymet happens D:
pages:
    stage: deploy
    dependencies: [pages_build]
    needs: [pages_build]
    script:
        - mkdir -p public
        - cp pages_build/* public
    rules:
        - if: $CI_COMMIT_BRANCH == "main"
          when: always
        - when: never
    artifacts:
        paths:
            - public
        expire_in: 1h


pages_preview:
    stage: deploy
    dependencies: [pages_build]
    needs: [pages_build]
    script:
        - mkdir -p public
        - cp pages_build/* public
    artifacts:
        paths:
            - public
    rules:
        - if: $CI_MERGE_REQUEST_IID
    environment:
        name: preview/${CI_PROJECT_NAME}/${CI_COMMIT_REF_NAME}
        url: https://${CI_PROJECT_NAMESPACE}.gitlab.io/-/${CI_PROJECT_NAME}/-/jobs/${CI_JOB_ID}/artifacts/public/index.html
        on_stop: pages_preview_stop


pages_preview_stop:
  stage: deploy
  rules:
    - if: $CI_MERGE_REQUEST_IID
      when: manual
  allow_failure: true
  environment:
    name: preview/${CI_PROJECT_NAME}/${CI_COMMIT_REF_NAME}
    action: stop
  script:
    - echo "👋 bye"


upload:
  dependencies: [windows_build, linux_build]
  needs: [windows_build, linux_build]
  stage: upload
  image: curlimages/curl:latest
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - |
      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file surfer_win.zip "${PACKAGE_REGISTRY_URL}/surfer_win_${CI_COMMIT_TAG}.zip"
    - |
      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file surfer_linux.zip "${PACKAGE_REGISTRY_URL}/surfer_linux_${CI_COMMIT_TAG}.zip"
    - |
      curl --header "JOB-TOKEN: ${CI_JOB_TOKEN}" --upload-file surfer_macos-aarch64.zip "${PACKAGE_REGISTRY_URL}/surfer_macos-aarch64_${CI_COMMIT_TAG}.zip"

release:
  # Caution, as of 2021-02-02 these assets links require a login, see:
  # https://gitlab.com/gitlab-org/gitlab/-/issues/299384
  dependencies: [upload]
  needs: [upload]
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - |
      release-cli create --name "Release $CI_COMMIT_TAG" --tag-name $CI_COMMIT_TAG \
        --assets-link "{\"name\":\"surfer_win_${CI_COMMIT_TAG}.zip\",\"url\":\"${PACKAGE_REGISTRY_URL}/surfer_win_${CI_COMMIT_TAG}.zip\"}" \
        --assets-link "{\"name\":\"surfer_linux_${CI_COMMIT_TAG}.zip\",\"url\":\"${PACKAGE_REGISTRY_URL}/surfer_linux_${CI_COMMIT_TAG}.zip\"}" \
        --assets-link "{\"name\":\"surfer_macos-aarch64_${CI_COMMIT_TAG}.zip\",\"url\":\"${PACKAGE_REGISTRY_URL}/surfer_macos-aarch64_${CI_COMMIT_TAG}.zip\"}"
