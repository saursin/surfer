# Changelog

All notable changes to this project will be documented in this file.

Surfer is currently unstable and all 0.x releases are expected to contain
breaking changes. Releases are mainly symbolic and are done on a six-week
release cycle. Every six weeks, the current master branch is tagged and
released as a new version.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).



## [0.1.0] - 2023-03-07

Initial numbered version


[Unreleased]: https://gitlab.com/surfer-project/surfer/-/compare/v0.1.0...master
[0.1.0]: https://gitlab.com/surfer-project/surfer/-/tree/v0.1.0
